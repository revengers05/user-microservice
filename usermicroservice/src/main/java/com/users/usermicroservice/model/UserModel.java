package com.users.usermicroservice.model;

public class UserModel {
  private int userId;

  private String email;

  private String name;

  private String password;

  private String type;

  public UserModel() {}

  public UserModel(int userId, String email, String name, String password, String type) {
    this.userId = userId;
    this.email = email;
    this.name = name;
    this.password = password;
    this.type = type;
  }
  public UserModel(int userId,String email,String name){
    this.userId=userId;
    this.email=email;
    this.name=name;
  }
  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPassword() {
    return password;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getUserId() {
    return userId;
  }

}
