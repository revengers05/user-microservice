package com.users.usermicroservice.controller;

import com.users.usermicroservice.model.UserModel;
import com.users.usermicroservice.service.UserServiceImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/v1.0/users")
public class UserController {
  @Autowired private UserServiceImpl userService;
  @Autowired private RedisTemplate<String, String> redisTemplate;

  @GetMapping("/getDetails/{userId}")
  public ResponseEntity<Object> userDetails(
      @PathVariable int userId, @RequestHeader("Authorization") String token) {
    // Validate the token before processing the request
    if (!validateToken(token)) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid token");
    }

    UserModel user = userService.userDetails(userId);
    if (user != null) {
      return ResponseEntity.ok(user);
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }
  }

  @PostMapping("/login")
  public ResponseEntity<Object> verify(@RequestBody UserModel userModel) {
    Optional<UserModel> users =
        userService.verify(userModel.getEmail(), userModel.getPassword(), userModel.getType());
    if (users.isPresent()) {
      System.out.println("Inside Login");
      Key key = Keys.secretKeyFor(SignatureAlgorithm.HS512);
      String token = generateToken(userModel.getEmail(), key);
      Integer userId = users.get().getUserId();
      System.out.println(userId);
      Map<String, Object> response = new HashMap<>();
      response.put("token", token);
      response.put("userId", userId);

      return ResponseEntity.ok(response);
    } else {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid credentials");
    }
  }

  @PostMapping("/updateUser")
  public ResponseEntity<Object> updateUser(
      @RequestBody UserModel userModel, @RequestHeader("Authorization") String token) {
    // Validate the token before processing the request
    if (!validateToken(token)) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid token");
    }

    boolean isSuccess = userService.updateUser(userModel);
    if (isSuccess) {
      return ResponseEntity.ok("User updated successfully");
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }
  }

  @PostMapping("/signup")
  public ResponseEntity<Object> createUser(@RequestBody UserModel userModel) {
    log.info("Hi");
    boolean isSuccess = userService.create(userModel);
    if (isSuccess) {
      System.out.println("HELLO");
      return ResponseEntity.ok("User created successfully");
    } else {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST)
          .body("User with this email already exists");
    }
  }

  @PutMapping("/updatepassword")
  public ResponseEntity<Object> updatePassword(
      @RequestBody UserModel userModel, @RequestHeader("Authorization") String token) {
    // Validate the token before processing the request
    if (!validateToken(token)) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid token");
    }

    boolean isSuccess = userService.updatePassword(userModel);
    if (isSuccess) {
      return ResponseEntity.ok("Password updated successfully");
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }
  }

  @DeleteMapping("/deleteUser/{userId}")
  public ResponseEntity<Object> deleteUser(
      @PathVariable int userId, @RequestHeader("Authorization") String token) {
    // Validate the token before processing the request
    if (!validateToken(token)) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid token");
    }

    UserModel userModel = new UserModel();
    userModel.setUserId(userId);
    boolean isSuccess = userService.deleteUser(userModel);
    if (isSuccess) {
      return ResponseEntity.ok("User deleted successfully");
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }
  }

  private String generateToken(String email, Key key) {
    long expirationTime = 10 * 60 * 1000;
    String token =
        Jwts.builder()
            .setSubject(email) // Set email as the subject instead of username
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
            .signWith(SignatureAlgorithm.HS512, key)
            .compact();
    redisTemplate.opsForValue().set(token, email, expirationTime, TimeUnit.MILLISECONDS);
    return token;
  }

  private boolean validateToken(String token) {
    String storedEmail = redisTemplate.opsForValue().get(token);
    return storedEmail == null || !storedEmail.equals(getUsernameFromToken(token));
  }

  private String getUsernameFromToken(String token) {
    return Jwts.parserBuilder()
        .setSigningKey(Keys.secretKeyFor(SignatureAlgorithm.HS512))
        .build()
        .parseClaimsJws(token)
        .getBody()
        .getSubject();
  }
}
