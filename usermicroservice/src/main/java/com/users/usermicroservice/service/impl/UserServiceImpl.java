package com.users.usermicroservice.service;

import com.users.usermicroservice.entity.Users;
import com.users.usermicroservice.model.UserModel;
import com.users.usermicroservice.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;
@Slf4j
@Service
public class UserService implements com.users.usermicroservice.service.impl.UserServiceImpl {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserModel userDetails(int userId) {
        Optional<Users> users = userRepository.findById(userId);
        return users.map(user -> new UserModel(user.getUserId(), user.getEmail(), user.getName(), user.getPassword(), user.getType()))
                .orElse(null);
    }

    @Override
    public Optional<UserModel> verify(String email, String password, String type) {
        Optional<Users> optionalUser = userRepository.findByEmailAndPasswordAndType(email, password, type);
        Optional<UserModel> optionalUserModel= Optional.of(new UserModel());
        optionalUserModel.get().setUserId(optionalUser.get().getUserId());
        optionalUserModel.get().setEmail(optionalUser.get().getEmail());
        optionalUserModel.get().setName(optionalUser.get().getName());
        optionalUserModel.get().setType(optionalUser.get().getType());
        optionalUserModel.get().setPassword(optionalUser.get().getPassword());
        log.info("I am here");
        return optionalUserModel;
    }

    @Override
    public boolean updateUser(UserModel userModel) {
        Optional<Users> optionalUser = userRepository.findById(userModel.getUserId());
        return optionalUser.map(user -> {
            user.setEmail(userModel.getEmail());
            user.setName(userModel.getName());
            user.setPassword(userModel.getPassword());
            user.setType(userModel.getType());
            userRepository.save(user);
            return true;
        }).orElse(false);
    }

    @Override
    public boolean create(UserModel userModel) {
        if (userRepository.findByEmailAndType(userModel.getEmail(),userModel.getType()).isPresent()) {
            return false;
        }

        Users user = new Users(
                userModel.getUserId(),
                userModel.getEmail(),
                userModel.getName(),
                userModel.getPassword(),
                userModel.getType());
        userRepository.save(user);
        return true;
    }

    @Override
    public boolean updatePassword(UserModel userModel) {
        Optional<Users> optionalUser = userRepository.findById(userModel.getUserId());
        return optionalUser.map(user -> {
            user.setPassword(userModel.getPassword());
            userRepository.save(user);
            return true;
        }).orElse(false);
    }

    @Override
    public boolean deleteUser(UserModel userModel) {
        try {
            userRepository.deleteByUserId(userModel.getUserId());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
