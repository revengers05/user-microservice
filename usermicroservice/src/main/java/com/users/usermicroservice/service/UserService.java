package com.users.usermicroservice.service;

import com.users.usermicroservice.entity.Users;
import com.users.usermicroservice.model.UserModel;
import com.users.usermicroservice.repository.UserRepository;
import com.users.usermicroservice.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class UserService implements UserServiceImpl {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserModel userDetails(int userId) {
        Optional<Users> users = userRepository.findById(userId);
        return users.map(user -> new UserModel(user.getUserId(), user.getEmail(), user.getName(), user.getPassword(), user.getType()))
                .orElse(null);
    }

    @Override
    public boolean verify(String email, String password, String type) {
        Optional<Users> optionalUser = userRepository.findByEmailAndPasswordAndType(email, password, type);
        return optionalUser.isPresent();
    }

    @Override
    public boolean updateUser(UserModel userModel) {
        Optional<Users> optionalUser = userRepository.findById(userModel.getUserId());
        return optionalUser.map(user -> {
            user.setEmail(userModel.getEmail());
            user.setName(userModel.getName());
            user.setPassword(userModel.getPassword());
            user.setType(userModel.getType());
            userRepository.save(user);
            return true;
        }).orElse(false);
    }

    @Override
    public boolean create(UserModel userModel) {
        if (userRepository.findByEmailAndType(userModel.getEmail(),userModel.getType()).isPresent()) {
            return false;
        }

        Users user = new Users(
                userModel.getUserId(),
                userModel.getEmail(),
                userModel.getName(),
                userModel.getPassword(),
                userModel.getType());
        userRepository.save(user);
        return true;
    }

    @Override
    public boolean updatePassword(UserModel userModel) {
        Optional<Users> optionalUser = userRepository.findById(userModel.getUserId());
        return optionalUser.map(user -> {
            user.setPassword(userModel.getPassword());
            userRepository.save(user);
            return true;
        }).orElse(false);
    }

    @Override
    public boolean deleteUser(UserModel userModel) {
        try {
            userRepository.deleteByUserId(userModel.getUserId());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
