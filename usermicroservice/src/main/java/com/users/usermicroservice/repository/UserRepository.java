package com.users.usermicroservice.repository;

import com.users.usermicroservice.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Users, Integer> {

  Optional<Users> findByEmailAndPasswordAndType(String email, String password, String type);

  Optional<Users> findByEmailAndType(String email, String type);

  Optional<Users> findByEmail(String email);

  @Transactional
  void deleteByUserId(int userId);
}
